/*
Bones Scripts File
Author: Eddie Machado

This file should contain any js scripts you want to add to the site.
Instead of calling it in the header or throwing it inside wp_head()
this file will be called automatically in the footer so as not to
slow the page load.

*/

// IE8 ployfill for GetComputed Style (for Responsive Script below)
if (!window.getComputedStyle) {
    window.getComputedStyle = function(el, pseudo) {
        this.el = el;
        this.getPropertyValue = function(prop) {
            var re = /(\-([a-z]){1})/g;
            if (prop == 'float') prop = 'styleFloat';
            if (re.test(prop)) {
                prop = prop.replace(re, function () {
                    return arguments[2].toUpperCase();
                });
            }
            return el.currentStyle[prop] ? el.currentStyle[prop] : null;
        }
        return this;
    }
}

// as the page loads, call these scripts
jQuery(document).ready(function($) {
    new WOW().init();
    $('.loading').removeClass('loading').addClass('loaded');
     // $("body").css("opacity", "1");
    $("html, .post_list_with_scroll").niceScroll({
        cursorwidth : 6,

        cursorcolor: '#999',
        cursorborder: 'none',
        cursoropacitymin: 0,
        cursoropacitymax: 0.8
    });

    /*
    Responsive jQuery is a tricky thing.
    There's a bunch of different ways to handle
    it, so be sure to research and find the one
    that works for you best.
    */

    /* getting viewport width */
    var responsive_viewport = $(window).width();

    /* if is below 481px */
    if (responsive_viewport < 481) {

    } /* end smallest screen */

    /* if is larger than 481px */
    if (responsive_viewport > 481) {

    } /* end larger than 481px */

    /* if is above or equal to 768px */
    if (responsive_viewport >= 768) {

        /* load gravatars */
        $('.comment img[data-gravatar]').each(function(){
            $(this).attr('src',$(this).attr('data-gravatar'));
        });

    }

    /* off the bat large screen actions */
    if (responsive_viewport > 1030) {

    }


	// add all your scripts here
	$('.carousel').carousel();


    if(
        $( "body" ).hasClass( "page-id-114" ) ||
        $( "body" ).hasClass( "page-id-112" ) ||
        $( "body" ).hasClass( "page-id-110" ) ||
        $( "body" ).hasClass( "page-id-108" )
      ) {
            $('#menu-item-105').addClass('open');
    }

    if(
        $( "body" ).hasClass( "page-id-17" ) ||
        $( "body" ).hasClass( "page-id-96" ) ||
        $( "body" ).hasClass( "page-id-98" )
      ) {
            $('#menu-item-181').addClass('open');
    }


    var left_offset = (($(window).width())/2)-100;
    $('#carousel-strategy-area .carousel-indicators').css('left',left_offset+'px');
    $('#carousel-strategy-area .carousel-inner').css('display','none');
    $('#carousel-strategy-area .carousel-indicators').click(function(){
        $( this ).animate({
            width: "400px",
            left: left_offset-100+'px'
        },50,function(){

            $('#carousel-strategy-area .carousel-inner').slideDown( "slow", function() {
                // Animation complete.
            });
            //$('#carousel-strategy-area .carousel-inner').css('display','block');
        });
    });

    $('#scrollToTop').click(function(){
        $('html,body').animate({ scrollTop: 0 }, 'slow');
        return false;
    })

    var submenu_offset= 0-$(window).width()/1.6;
    var submenu_offset_about_us= 0-$(window).width()/2.0;
    $('#menu-item-105.menu-item-has-children .dropdown-menu').css(
        {
            'width':$(window).width()+400,
            'left':submenu_offset
        }
    );

    $('#menu-item-181.menu-item-has-children .dropdown-menu').css(
        {

            'width':$(window).width()+400,
            'left':submenu_offset_about_us
        }
    );

    $('.post-519 li strong').append('<i class="fa fa-play"></i>');
    $('.post-519 li strong').click(function(){

        if ($(this).find('i').hasClass('fa-play')) {
            $(this).find('i').removeClass('fa-play').addClass('fa-pause');
        } else {
            $(this).find('i').removeClass('fa-pause').addClass('fa-play');
        }

        $(this).toggleClass('active');

        $(this).closest("li").find("p").slideToggle('slow');
        $(this).closest("li").find("ol").slideToggle('slow');
    })

    $('#list_toggle').click(function(e){
        e.preventDefault();
        $('.recent_posts_wrapper_inner').slideToggle();
        if ($(this).find('i').hasClass('fa-angle-double-up')) {
            $(this).find('i').removeClass('fa-angle-double-up').addClass('fa-angle-double-down');
        } else {
            $(this).find('i').removeClass('fa-angle-double-down').addClass('fa-angle-double-up');
        }
    });

    $('#nav1 button.navbar-toggle').click(function(){
        $( "#nav1" ).toggleClass('navbar-invisible');
    });
    $('#other-menu button.navbar-toggle').click(function(){
        if ($( "#other-menu" ).hasClass('navbar-absolute-top')) {
            if ($(this).hasClass('collapsed')) {
                $( "#other-menu" ).removeClass('navbar-invisible');
            }
            else $( "#other-menu" ).addClass('navbar-invisible');
        };

    });
    /*$('#bilkisvideoholder').magnificPopup({
        type: 'iframe'
        // other options
    });*/

/*
    $('#bilkisvideoholder').magnificPopup({
        type: 'inline',
        preloader: true,
        modal: true,
        callbacks: {
            open: function() {
                $("video.bilqis-story")[0].play();
            },
            close: function() {
                $("video.bilqis-story")[0].pause();
            }
            // e.t.c.
        }
    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $('#bilkisvideoholder').magnificPopup.close();
    });*/


    $('.open-popup-link').magnificPopup({
        type:'inline',

        midClick: true, // Allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source in href.
        callbacks: {
            open: function() {
                $("#bilqis-story")[0].play();
            },
            close: function() {
                $("#bilqis-story")[0].pause();
            }
            // e.t.c.
        }

    });



    $('#button_group_home_newsevents_filter input[name=postthumboptions]').on('change', function() {
        if ($('input[name=postthumboptions]:checked', '#button_group_home_newsevents_filter').val() == 'events') {
            $('#slider2_container').parent().hide().removeClass('hide').fadeIn('fast');
            $('#slider3_container').parent().show().addClass('hide').fadeOut('fast');
            // alert("events");
        }
        else {
            $('#slider3_container').parent().hide().removeClass('hide').fadeIn('fast');
            $('#slider2_container').parent().show().addClass('hide').fadeOut('fast');
        }
    });


    $('#button_group_list_newsevent_filter input[name=postthumboptions]').on('change', function() {


        if ($(this).is(':checked')) {
            if (this.value == 'events') {

                $('#optionnews').prop(false);
                $('.btn-news').removeClass('active');
                $('.btn-news').find('i.fa').removeClass('fa-check-square-o').addClass('fa-square-o');
                $('.newseventblock.'+this.value).find('.newsevent_overlay').addClass('hidden');
                $('.newseventblock.news').find('.newsevent_overlay').removeClass('hidden');
            }
            else{

                $('.optionevents').prop(false);
                $('.btn-events').removeClass('active');
                $('.btn-events').find('i.fa').removeClass('fa-check-square-o').addClass('fa-square-o');
                $('.newseventblock.'+this.value).find('.newsevent_overlay').addClass('hidden');
                $('.newseventblock.events').find('.newsevent_overlay').removeClass('hidden');
            }
            // alert(this.value);
            $(this).parent().find('i.fa').removeClass('fa-square-o').addClass('fa-check-square-o');
            // $('.newseventblock.'+this.value).find('.newsevent_overlay').addClass('hidden');

        }
        else {
            // alert('unchecked '+this.value);
            $(this).parent().find('i.fa').removeClass('fa-check-square-o').addClass('fa-square-o');
            // $('.newseventblock.'+this.value).find('.newsevent_overlay').removeClass('hidden');
            $('.newseventblock').find('.newsevent_overlay').addClass('hidden');
        }
    });


    $('#other-menu').click( function() {
        // $(window).scrollTo('#other-menu');
        console.log('scroll to nav#other-menu')
    });


    $('.searchform input#search').focus(function(){
        $('.searchform').css({'border':'1px solid #fff'});
        $('.current-menu-parent').addClass('open');
    }).blur(function() {

        $('.searchform').css({'border':'1px solid transparent'});

    });

    $('.strategies-closer').click(function(){

            $('#carousel-strategy-area .carousel-inner').slideUp( "slow", function() {
                $('#carousel-strategy-area .carousel-indicators').css({'left':left_offset+'px','width':'200px'});
            });
    })


    if ($('.pager li').is(':empty')){
        $(this).css('visibility','hidden');
    }





}); /* end of as page load scripts */

/*! A fix for the iOS orientationchange zoom bug.
 Script by @scottjehl, rebound by @wilto.
 MIT License.
*/
(function(w){
	// This fix addresses an iOS bug, so return early if the UA claims it's something else.
	if( !( /iPhone|iPad|iPod/.test( navigator.platform ) && navigator.userAgent.indexOf( "AppleWebKit" ) > -1 ) ){ return; }
    var doc = w.document;
    if( !doc.querySelector ){ return; }
    var meta = doc.querySelector( "meta[name=viewport]" ),
        initialContent = meta && meta.getAttribute( "content" ),
        disabledZoom = initialContent + ",maximum-scale=1",
        enabledZoom = initialContent + ",maximum-scale=10",
        enabled = true,
		x, y, z, aig;
    if( !meta ){ return; }
    function restoreZoom(){
        meta.setAttribute( "content", enabledZoom );
        enabled = true; }
    function disableZoom(){
        meta.setAttribute( "content", disabledZoom );
        enabled = false; }
    function checkTilt( e ){
		aig = e.accelerationIncludingGravity;
		x = Math.abs( aig.x );
		y = Math.abs( aig.y );
		z = Math.abs( aig.z );
		// If portrait orientation and in one of the danger zones
        if( !w.orientation && ( x > 7 || ( ( z > 6 && y < 8 || z < 8 && y > 6 ) && x > 5 ) ) ){
			if( enabled ){ disableZoom(); } }
		else if( !enabled ){ restoreZoom(); } }
	w.addEventListener( "orientationchange", restoreZoom, false );
	w.addEventListener( "devicemotion", checkTilt, false );
})( this );


jQuery(document).ready(function( $ ) {

    var content=$("#content-1"),autoScrollTimer=30000,autoScrollTimerAdjust,autoScroll;

    content.mCustomScrollbar({
        scrollButtons:{enable:true},
        callbacks:{
            whileScrolling:function(){
                autoScrollTimerAdjust=autoScrollTimer*this.mcs.topPct/500;
            },
            onScroll:function(){
                if($(this).data("mCS").trigger==="internal"){AutoScrollOff();}
            }
        }
    });

    content.addClass("auto-scrolling-on auto-scrolling-to-bottom");
    AutoScrollOn("bottom");

    $(".auto-scrolling-toggle").click(function(e){
        e.preventDefault();
        if(content.hasClass("auto-scrolling-on")){
            AutoScrollOff();
        }else{
            if(content.hasClass("auto-scrolling-to-top")){
                AutoScrollOn("top",autoScrollTimerAdjust);
            }else{
                AutoScrollOn("bottom",autoScrollTimer-autoScrollTimerAdjust);
            }
        }
    });

    /*function AutoScrollOn(to,timer){
        if(!timer){timer=autoScrollTimer;}
        content.addClass("auto-scrolling-on").mCustomScrollbar("scrollTo",to,{scrollInertia:timer,scrollEasing:"easeInOutSmooth"});
        autoScroll=setTimeout(function(){
            if(content.hasClass("auto-scrolling-to-top")){
                AutoScrollOn("bottom",autoScrollTimer-autoScrollTimerAdjust);
                content.removeClass("auto-scrolling-to-top").addClass("auto-scrolling-to-bottom");
            }else{
                AutoScrollOn("top",autoScrollTimerAdjust);
                content.removeClass("auto-scrolling-to-bottom").addClass("auto-scrolling-to-top");
            }
        },timer);
    }*/

    function AutoScrollOff(){
        clearTimeout(autoScroll);
        content.removeClass("auto-scrolling-on").mCustomScrollbar("stop");
    }



});


// 1024/360

// 700/480