<?php
/**
 * Created by PhpStorm.
 * User: bazlur
 * Date: 5/11/15
 * Time: 2:23 PM
 */

/*****************************************************/


/**
 * Adds a box to the main column on the Post and Page edit screens.
 */
function make_featured_add_meta_box() {

    $screens = array( 'post', 'page' );

    foreach ( $screens as $screen ) {

        add_meta_box(
            'make_featured_sectionid',
            __( 'Post Meta Options', 'make_featured_textdomain' ),
            'make_featured_meta_box_callback',
            $screen
        );
    }
}
add_action( 'add_meta_boxes', 'make_featured_add_meta_box' );

/**
 * Prints the box content.
 *
 * @param WP_Post $post The object for the current post/page.
 */
function make_featured_meta_box_callback( $post ) {

    // Add a nonce field so we can check for it later.
    wp_nonce_field( 'make_featured_meta_box', 'make_featured_meta_box_nonce' );

    /*
     * Use get_post_meta() to retrieve an existing value
     * from the database and use the value for the form.
     */
    $value = get_post_meta( $post->ID, '_featured_meta_value_key', true );
    $pf = get_post_meta( $post->ID, '_post_featured', true );



    $checked = ($pf == 'on')? "checked = 'checked'":"";
    echo '<input type="checkbox" id="make_post_featured" name="make_post_featured" '.$checked.' />';
    echo '<label for="make_post_featured">';
    _e( 'Make Featured', 'make_featured_textdomain' );
    echo '</label> ';
}

/**
 * When the post is saved, saves our custom data.
 *
 * @param int $post_id The ID of the post being saved.
 */
function make_featured_save_meta_box_data( $post_id ) {

    /*
     * We need to verify this came from our screen and with proper authorization,
     * because the save_post action can be triggered at other times.
     */

    // Check if our nonce is set.
   /* if ( ! isset( $_POST['make_featured_meta_box_nonce'] ) ) {
        return;
    }

    // Verify that the nonce is valid.
    if ( ! wp_verify_nonce( $_POST['make_featured_meta_box_nonce'], 'make_featured_meta_box' ) ) {
        return;
    }*/

    // If this is an autosave, our form has not been submitted, so we don't want to do anything.
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return;
    }

    // Check the user's permissions.
    if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {

        if ( ! current_user_can( 'edit_page', $post_id ) ) {
            return;
        }

    } else {

        if ( ! current_user_can( 'edit_post', $post_id ) ) {
            return;
        }
    }

    /* OK, it's safe for us to save the data now. */


    // Sanitize user input.
    //$my_data = sanitize_text_field( $_POST['make_featured_new_field'] );
    $featured_data = $_POST['make_post_featured']?'on':'off';

    // Update the meta field in the database.
    //update_post_meta( $post_id, '_featured_meta_value_key', $my_data );
    update_post_meta( $post_id, '_post_featured', $featured_data );
}
add_action( 'save_post', 'make_featured_save_meta_box_data' );