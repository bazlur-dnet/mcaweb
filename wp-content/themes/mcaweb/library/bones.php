<?php
/* Welcome to Bones :)
This is the core Bones file where most of the
main functions & features reside. If you have
any custom functions, it's best to put them
in the functions.php file.

Developed by: Eddie Machado
URL: http://themble.com/bones/
*/

/*********************
LAUNCH BONES
Let's fire off all the functions
and tools. I put it up here so it's
right up top and clean.
*********************/

// we're firing all out initial functions at the start
add_action( 'after_setup_theme', 'bones_ahoy', 16 );

function bones_ahoy() {

    // launching operation cleanup
    add_action( 'init', 'bones_head_cleanup' );
    // remove WP version from RSS
    add_filter( 'the_generator', 'bones_rss_version' );
    // remove pesky injected css for recent comments widget
    add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );
    // clean up comment styles in the head
    add_action( 'wp_head', 'bones_remove_recent_comments_style', 1 );
    // clean up gallery output in wp
    add_filter( 'gallery_style', 'bones_gallery_style' );

    // enqueue base scripts and styles
    add_action( 'wp_enqueue_scripts', 'bones_scripts_and_styles', 999 );
    // ie conditional wrapper

    // launching this stuff after theme setup
    bones_theme_support();

    // adding sidebars to Wordpress (these are created in functions.php)
    add_action( 'widgets_init', 'bones_register_sidebars' );
    // adding the bones search form (created in functions.php)
    // add_filter( 'get_search_form', 'bones_wpsearch' );

    // cleaning up random code around images
    add_filter( 'the_content', 'bones_filter_ptags_on_images' );
    // cleaning up excerpt
    add_filter( 'excerpt_more', 'bones_excerpt_more' );

} /* end bones ahoy */

/*********************
WP_HEAD GOODNESS
The default wordpress head is
a mess. Let's clean it up by
removing all the junk we don't
need.
*********************/

function bones_head_cleanup() {
	// category feeds
	// remove_action( 'wp_head', 'feed_links_extra', 3 );
	// post and comment feeds
	// remove_action( 'wp_head', 'feed_links', 2 );
	// EditURI link
	remove_action( 'wp_head', 'rsd_link' );
	// windows live writer
	remove_action( 'wp_head', 'wlwmanifest_link' );
	// index link
	remove_action( 'wp_head', 'index_rel_link' );
	// previous link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
	// start link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
	// links for adjacent posts
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
	// WP version
	remove_action( 'wp_head', 'wp_generator' );
	// remove WP version from css
	add_filter( 'style_loader_src', 'bones_remove_wp_ver_css_js', 9999 );
	// remove Wp version from scripts
	add_filter( 'script_loader_src', 'bones_remove_wp_ver_css_js', 9999 );

} /* end bones head cleanup */

// remove WP version from RSS
function bones_rss_version() { return ''; }

// remove WP version from scripts
function bones_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}

// remove injected CSS for recent comments widget
function bones_remove_wp_widget_recent_comments_style() {
   if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
      remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
   }
}

// remove injected CSS from recent comments widget
function bones_remove_recent_comments_style() {
  global $wp_widget_factory;
  if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
    remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
  }
}

// remove injected CSS from gallery
function bones_gallery_style($css) {
  return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
}


/*********************
SCRIPTS & ENQUEUEING
*********************/

// loading modernizr and jquery, and reply script
function bones_scripts_and_styles() {
  global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way
  if (!is_admin()) {

    // js bootstrap
    // download a custom file @ getbootstrap.com/customize/ if you don't want all js components
    wp_register_script( 'bones-bootstrap', get_template_directory_uri() . '/library/js/libs/bootstrap.min.js', array(), '3.0.0', true );

    // modernizr (without media query polyfill)
    wp_register_script( 'bones-modernizr', get_template_directory_uri() . '/library/js/libs/modernizr.custom.min.js', array(), '2.5.3', false );



      // register main stylesheet
    wp_register_style( 'bones-stylesheet', get_template_directory_uri() . '/library/css/style.css', array(), '', 'all' );
    wp_register_style( 'mcaweb-stylesheet', get_template_directory_uri() . '/library/css/mcawebstyle.css', array(), '', 'all' );


    wp_register_style( 'magnific-popup', get_template_directory_uri() . '/library/js/libs/magnific-popup/magnific-popup.css', array(), '', 'all' );
    // ie-only style sheet
    wp_register_style( 'bones-ie-only', get_template_directory_uri() . '/library/css/ie.css', array(), '' );
    wp_register_style( 'animate-stylesheet', get_template_directory_uri() . '/library/css/animate.css', array(), '', 'all' );

      wp_register_style( 'customscrollbar-stylesheet', get_template_directory_uri() . '/library/css/jquery.mCustomScrollbar.min.css', array(), '', 'all' );

    // FitVid (responsive video)
    wp_register_script( 'fitvids', get_template_directory_uri() . '/library/js/libs/FitVids.js-master/jquery.fitvids.js', array('jquery'), '', TRUE );
    wp_register_script( 'fitvids-xtra', get_template_directory_uri() . '/library/js/fitvid.js', array(), '', TRUE );

    wp_register_script( 'magnific-popup', get_template_directory_uri() . '/library/js/libs/magnific-popup/jquery.magnific-popup.min.js', array('jquery'), '', TRUE );
    // comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
		wp_enqueue_script( 'comment-reply' );
    }

    //adding scripts file in the footer
    wp_register_script( 'slider-js', get_template_directory_uri() . '/library/js/sliderscripts.js', array( 'jquery' ), '', true );
    wp_register_script( 'bones-js', get_template_directory_uri() . '/library/js/scripts.js', array( 'jquery' ), '', true );
    wp_register_script( 'jssor-js', get_template_directory_uri() . '/library/js/jssor.slider.min.js', array( 'jquery' ), '', true );
    wp_register_script( 'nicescroll', get_template_directory_uri() . '/library/js/jquery.nicescroll.min.js', array( 'jquery' ), '', true );
    wp_register_script( 'wow', get_template_directory_uri() . '/library/js/wow.min.js', array( 'jquery' ), '', true );

    wp_register_script( 'customscrollbar', get_template_directory_uri() . '/library/js/jquery.mCustomScrollbar.concat.min.js', array( 'jquery' ), '', true );


    // enqueue styles and scripts
    wp_enqueue_script( 'bones-modernizr' );
    wp_enqueue_style( 'customscrollbar-stylesheet' );
    wp_enqueue_style( 'bones-stylesheet' );
    wp_enqueue_style( 'bones-ie-only' );

    wp_enqueue_style('magnific-popup');

    wp_enqueue_style('mcaweb-stylesheet');
    wp_enqueue_style( 'animate-stylesheet' );

    $wp_styles->add_data( 'bones-ie-only', 'conditional', 'lt IE 9' ); // add conditional wrapper around ie stylesheet

    /*
    I recommend using a plugin to call jQuery
    using the google cdn. That way it stays cached
    and your site will load faster.
    */
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'bones-bootstrap' );
    wp_enqueue_script( 'fitvids');
    wp_enqueue_script( 'fitvids-xtra');
    wp_enqueue_script( 'jssor-js' );
    wp_enqueue_script( 'magnific-popup');

    wp_enqueue_script( 'bones-js' );
    wp_enqueue_script( 'nicescroll' );
    wp_enqueue_script( 'wow' );
    wp_enqueue_script( 'customscrollbar' );
    if(is_home() ) wp_enqueue_script( 'slider-js' );

  }
}

function print_my_inline_script() {
	if (is_home()) {
		if ( wp_script_is( 'jquery', 'done' ) ) {
			?>
			<script type="text/javascript">
			jQuery(document).ready(function( $ ) {

			    var nav = $('#other-menu');

			    $(window).scroll(function () {

			        var viewport_height = $('#slider1_container').height();
			        // var viewport_height = $(window).width()/2;
			        if (!nav.hasClass('navbar-nothome')){
			            if ($(this).scrollTop() > viewport_height-50) {
			                nav.find('.navbar-brand').removeClass('hidden');
			                nav.removeClass("navbar-absolute-top navbar-overthetop").removeClass("navbar-invisible").addClass("navbar-brand-color navbar-fixed-top");
			                // alert(viewport_height);
			            } else {

			                nav.find('.navbar-brand').addClass('hidden');
			                nav.removeClass("navbar-fixed-top navbar-brand-color").addClass("navbar-absolute-top navbar-invisible navbar-overthetop");
			                // alert(viewport_height);
			            }
			        }
			    });


                var _SlideshowTransitions = [
                    {$Duration:1700,$Opacity:2,$Brother:{$Duration:1000,$Opacity:2}}
                ];
                var _CaptionTransitions = [
                    {$Duration:700,$Opacity:2,$Brother:{$Duration:1000,$Opacity:2}},
                    {$Duration:900,x:-0.3,$Easing:{$Left:$JssorEasing$.$EaseInOutSine},$Opacity:3}
                ];
                // _CaptionTransitions[] = {$Duration:900,x:-0.1,$Easing:{$Left:$JssorEasing$.$EaseInOutSine},$Opacity:4};
                var options = {
                    $AutoPlay: true,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                    $PlayOrientation: 2,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                    $DragOrientation: 2,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                    $CaptionSliderOptions: {
                        $Class: $JssorCaptionSlider$,
                        $CaptionTransitions: _CaptionTransitions,
                        $PlayInMode: 1,
                        $PlayOutMode: 0
                    }
                };
                // var options = { $AutoPlay: true };
                var jssor_slider1 = new $JssorSlider$('slider1_container', options);





                //responsive code begin
                //you can remove responsive code if you don't want the slider to scale along with window
                function ScaleSlider() {
                    var windowWidth = $(window).width();

                    if (windowWidth) {
                        var windowHeight = $(window).height();
                        var originalWidth = jssor_slider1.$OriginalWidth();
                        var originalHeight = jssor_slider1.$OriginalHeight();

                        if (originalWidth / windowWidth > originalHeight / windowHeight) {
                            // jssor_slider1.$ScaleHeight(windowHeight);
                            jssor_slider1.$ScaleWidth(windowWidth);
                        }
                        else {
                            jssor_slider1.$ScaleWidth(windowWidth);
                        }
                    }
                    else
                        window.setTimeout(ScaleSlider, 30);
                }

                ScaleSlider();

                $(window).bind("load", ScaleSlider);
                $(window).bind("resize", ScaleSlider);
                $(window).bind("orientationchange", ScaleSlider);
                //responsive code end
                var _CaptionTransitions2 = [
                    {$Duration:700,$Opacity:2,$Brother:{$Duration:1000,$Opacity:2}},
                    {$Duration:900,x:-0.3,$Easing:{$Left:$JssorEasing$.$EaseInOutSine},$Opacity:3}
                ];
                var options2 = {
                    $AutoPlay: false,                                    //[Optional] Whether to auto play, to enable slideshow, this option must be set to true, default value is false
                    $AutoPlaySteps: 1,                                  //[Optional] Steps to go for each navigation request (this options applys only when slideshow disabled), the default value is 1
                    $AutoPlayInterval: 4000,                            //[Optional] Interval (in milliseconds) to go for next slide since the previous stopped if the slider is auto playing, default value is 3000
                    $PauseOnHover: 1,                               //[Optional] Whether to pause when mouse over if a slider is auto playing, 0 no pause, 1 pause for desktop, 2 pause for touch device, 3 pause for desktop and touch device, 4 freeze for desktop, 8 freeze for touch device, 12 freeze for desktop and touch device, default value is 1

                    $ArrowKeyNavigation: true,                          //[Optional] Allows keyboard (arrow key) navigation or not, default value is false
                    $SlideDuration: 360,                                //[Optional] Specifies default duration (swipe) for slide in milliseconds, default value is 500
                    $MinDragOffsetToSlide: 20,                          //[Optional] Minimum drag offset to trigger slide , default value is 20
                    $SlideWidth: 256,                                   //[Optional] Width of every slide in pixels, default value is width of 'slides' container
                    //$SlideHeight: 150,                                //[Optional] Height of every slide in pixels, default value is height of 'slides' container
                    $SlideSpacing: 0,                                   //[Optional] Space between each slide in pixels, default value is 0
                    $DisplayPieces: 5,                                  //[Optional] Number of pieces to display (the slideshow would be disabled if the value is set to greater than 1), the default value is 1
                    $ParkingPosition: 0,                              //[Optional] The offset position to park slide (this options applys only when slideshow disabled), default value is 0.
                    $UISearchMode: 1,                                   //[Optional] The way (0 parellel, 1 recursive, default value is 1) to search UI components (slides container, loading screen, navigator container, arrow navigator container, thumbnail navigator container etc).
                    $PlayOrientation: 1,                                //[Optional] Orientation to play slide (for auto play, navigation), 1 horizental, 2 vertical, 5 horizental reverse, 6 vertical reverse, default value is 1
                    $DragOrientation: 1,                                //[Optional] Orientation to drag slide, 0 no drag, 1 horizental, 2 vertical, 3 either, default value is 1 (Note that the $DragOrientation should be the same as $PlayOrientation when $DisplayPieces is greater than 1, or parking position is not 0)

                    $BulletNavigatorOptions: {                                //[Optional] Options to specify and enable navigator or not
                        $Class: $JssorBulletNavigator$,                       //[Required] Class to create navigator instance
                        $ChanceToShow: 2,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                        $AutoCenter: 0,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                        $Steps: 1,                                      //[Optional] Steps to go for each navigation request, default value is 1
                        $Lanes: 1,                                      //[Optional] Specify lanes to arrange items, default value is 1
                        $SpacingX: 0,                                   //[Optional] Horizontal space between each item in pixel, default value is 0
                        $SpacingY: 0,                                   //[Optional] Vertical space between each item in pixel, default value is 0
                        $Orientation: 1                                 //[Optional] The orientation of the navigator, 1 horizontal, 2 vertical, default value is 1
                    },

                    $ArrowNavigatorOptions: {
                        $Class: $JssorArrowNavigator$,              //[Requried] Class to create arrow navigator instance
                        $ChanceToShow: 1,                               //[Required] 0 Never, 1 Mouse Over, 2 Always
                        $AutoCenter: 2,                                 //[Optional] Auto center navigator in parent container, 0 None, 1 Horizontal, 2 Vertical, 3 Both, default value is 0
                        $Steps: 1                                       //[Optional] Steps to go for each navigation request, default value is 1
                    },
                    $CaptionSliderOptions: {
                        $Class: $JssorCaptionSlider$,
                        $CaptionTransitions: _CaptionTransitions2,
                        $PlayInMode: 0,
                        $PlayOutMode: 0
                    }
                };

                var jssor_slider2 = new $JssorSlider$("slider2_container", options2);
                var jssor_slider3 = new $JssorSlider$("slider3_container", options2);

                //responsive code begin
                //you can remove responsive code if you don't want the slider scales while window resizes
                function ScaleSlider2() {
                    var bodyWidth = document.body.clientWidth;
                    if (bodyWidth)
                    {
                        // jssor_slider2.$ScaleWidth(Math.min(bodyWidth, 1024));
                        // jssor_slider3.$ScaleWidth(Math.min(bodyWidth, 1024));
                        jssor_slider2.$ScaleWidth(bodyWidth);
                        jssor_slider3.$ScaleWidth(bodyWidth);
                    }
                    else
                        window.setTimeout(ScaleSlider2, 30);
                }
                ScaleSlider2();

                $(window).bind("load", ScaleSlider2);
                $(window).bind("resize", ScaleSlider2);
                $(window).bind("orientationchange", ScaleSlider2);


			});
			</script>
			<?php
		  }
	}
	if (is_single() && !is_home()) {
		if ( wp_script_is( 'jquery', 'done' ) ) {
			?>
			<script type="text/javascript">

			jQuery(document).ready(function( $ ) {

			    var nav = $('#other-menu');

			    $(window).scroll(function () {

			        var viewport_height = $('.feat-head').height();
			        // var viewport_height = $(window).width()/2;
			        if (nav.hasClass('navbar-nothome')){
			            if ($(this).scrollTop() > viewport_height-50) {
			                // nav.find('.navbar-brand').removeClass('hidden');
			                nav.removeClass("navbar-inverse navbar-invisible").addClass("navbar-brand-color");
			                // alert(viewport_height);
			            } else {

			                // nav.find('.navbar-brand').addClass('hidden');
			                nav.removeClass("navbar-brand-color").addClass("navbar-inverse navbar-invisible");
			                // alert(viewport_height);
			            }
			        }
			    });
			});
			</script>
			<?php
		  }
	}
}
add_action( 'wp_footer', 'print_my_inline_script' );

/*********************
THEME SUPPORT
*********************/

// Adding WP 3+ Functions & Theme Support
function bones_theme_support() {

	// wp thumbnails (sizes handled in functions.php)
	add_theme_support( 'post-thumbnails' );

	// default thumb size
	set_post_thumbnail_size(125, 125, true);

	// wp custom background (thx to @bransonwerner for update)
	add_theme_support( 'custom-background',
	    array(
	    'default-image' => '',  // background image default
	    'default-color' => '', // background color default (dont add the #)
	    'wp-head-callback' => '_custom_background_cb',
	    'admin-head-callback' => '',
	    'admin-preview-callback' => ''
	    )
	);

	// rss thingy
	add_theme_support('automatic-feed-links');

	// to add header image support go here: http://themble.com/support/adding-header-background-image-support/

	// adding post format support
	add_theme_support( 'post-formats',
		array(
			'aside',             // title less blurb
			'gallery',           // gallery of images
			'link',              // quick link to other site
			'image',             // an image
			'quote',             // a quick quote
			'status',            // a Facebook like status update
			'video',             // video
			'audio',             // audio
			'chat'               // chat transcript
		)
	);

	// wp menus
	add_theme_support( 'menus' );

	// registering wp3+ menus
	register_nav_menus(
		array(
			'main-nav' => __( 'The Main Menu', 'bonestheme' ),   // main nav in header
			'second-nav' => __( 'Secondary menu', 'bonestheme' ),   // main nav in header
		)
	);
} /* end bones theme support */


/*********************
MENUS & NAVIGATION
*********************/

// the main menu
function bones_main_nav() {
	// display the wp3 menu if available
    wp_nav_menu(array(
    	'container' => false,                           			// remove nav container
    	'container_class' => 'menu clearfix',           			// class of container (should you choose to use it)
    	'menu' => __( 'The Main Menu', 'bonestheme' ),  			// nav name
    	'menu_class' => 'nav navbar-nav navbar-right',  			// adding custom nav class
    	'theme_location' => 'main-nav',                 			// where it's located in the theme
    	'before' => '',                                 			// before the menu
      'after' => '',                                  			// after the menu
      'link_before' => '',                            			// before each link
      'link_after' => '',                             			// after each link
      'depth' => 2,                                   			// limit the depth of the nav
      'fallback_cb' => 'wp_bootstrap_navwalker::fallback',	// fallback
    	'walker' => new wp_bootstrap_navwalker()        			// for bootstrap nav
	));
} /* end bones main nav */

// this is the fallback for header menu
function bones_main_nav_fallback() {
	wp_page_menu( array(
		'show_home' => true,
    	'menu_class' => 'nav top-nav clearfix',      // adding custom nav class
		'include'     => '',
		'exclude'     => '',
		'echo'        => true,
        'link_before' => '',                            // before each link
        'link_after' => ''                             // after each link
	) );
}

function bones_second_nav() {
	// display the wp3 menu if available
    wp_nav_menu(array(
    	'container' => false,                           			// remove nav container
    	'container_class' => 'menu clearfix',           			// class of container (should you choose to use it)
    	'menu' => __( 'Secondary menu', 'bonestheme' ),  			// nav name
    	'menu_class' => 'nav navbar-nav navbar-right',  			// adding custom nav class
    	'theme_location' => 'second-nav',                 			// where it's located in the theme
    	'before' => '',                                 			// before the menu
      'after' => '',                                  			// after the menu
      'link_before' => '',                            			// before each link
      'link_after' => '',                             			// after each link
      'depth' => 2,                                   			// limit the depth of the nav
      'fallback_cb' => 'wp_bootstrap_navwalker::fallback',	// fallback
    	'walker' => new wp_bootstrap_navwalker()        			// for bootstrap nav
	));
} /* end bones main nav */

// this is the fallback for header menu
function bones_second_nav_fallback() {
	wp_page_menu( array(
		'show_home' => true,
    	'menu_class' => 'nav top-nav clearfix',      // adding custom nav class
		'include'     => '',
		'exclude'     => '',
		'echo'        => true,
        'link_before' => '',                            // before each link
        'link_after' => ''                             // after each link
	) );
}

// this is the fallback for footer menu
function bones_footer_links_fallback() {
	/* you can put a default here if you like */
}

/*********************
RELATED POSTS FUNCTION
*********************/

// Related Posts Function (call using bones_related_posts(); )
function bones_related_posts() {
	echo '<ul id="bones-related-posts">';
	global $post;
	$tags = wp_get_post_tags( $post->ID );
	if($tags) {
		foreach( $tags as $tag ) { 
			$tag_arr .= $tag->slug . ',';
		}
        $args = array(
        	'tag' => $tag_arr,
        	'numberposts' => 5, /* you can change this to show more */
        	'post__not_in' => array($post->ID)
     	);
        $related_posts = get_posts( $args );
        if($related_posts) {
        	foreach ( $related_posts as $post ) : setup_postdata( $post ); ?>
	           	<li class="related_post"><a class="entry-unrelated" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>
	        <?php endforeach; }
	    else { ?>
            <?php echo '<li class="no_related_post">' . __( 'No Related Posts Yet!', 'bonestheme' ) . '</li>'; ?>
		<?php }
	}
	wp_reset_query();
	echo '</ul>';
} /* end bones related posts function */

/*********************
PAGE NAVI
*********************/

// Numeric Page Navi (built into the theme by default)
function bones_page_navi($max_num_pages='') {
	global $wp_query;
	$bignum = 999999999;
	if (!isset( $max_num_pages ) ) {
		$max_num_pages = $wp_query->max_num_pages;
	}
	if ( $max_num_pages <= 1 )
	return;
	
	echo '<nav class="pagination">';
	
		echo paginate_links( array(
			'base' 			=> str_replace( $bignum, '%#%', esc_url( get_pagenum_link($bignum) ) ),
			'format' 		=> '',
			'current' 		=> max( 1, get_query_var('paged') ),
			'total' 		=> $max_num_pages,
			'prev_text' 	=> '&larr;',
			'next_text' 	=> '&rarr;',
			'type'			=> 'list',
			'end_size'		=> 3,
			'mid_size'		=> 3
		) );
	
	echo '</nav>';
} /* end page navi */

/*********************
RANDOM CLEANUP ITEMS
*********************/

// remove the p from around imgs (http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/)
function bones_filter_ptags_on_images($content){
   return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

// This removes the annoying […] to a Read More link
function bones_excerpt_more($more) {
	global $post;
	// edit here if you like
return '...</p><p><a class="excerpt-read-more btn btn-primary" href="'. get_permalink($post->ID) . '" title="'. __( 'Read', 'bonestheme' ) . get_the_title($post->ID).'">'. __( 'Read More', 'bonestheme' ) .'</a>';
}

/*
 * This is a modified the_author_posts_link() which just returns the link.
 *
 * This is necessary to allow usage of the usual l10n process with printf().
 */
function bones_get_the_author_posts_link() {
	global $authordata;
	if ( !is_object( $authordata ) )
		return false;
	$link = sprintf(
		'<a href="%1$s" title="%2$s" rel="author">%3$s</a>',
		get_author_posts_url( $authordata->ID, $authordata->user_nicename ),
		esc_attr( sprintf( __( 'Posts by %s' ), get_the_author() ) ), // No further l10n needed, core will take care of this one
		get_the_author()
	);
	return $link;
}

?>
