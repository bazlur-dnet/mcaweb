<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

	<head>
		<meta charset="utf-8">

		<?php // Google Chrome Frame for IE ?>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<title><?php if (is_front_page()) { bloginfo('name'); } else { wp_title(''); } ?></title>

		<?php // mobile meta (hooray!) ?>
		<meta name="HandheldFriendly" content="True">
		<meta name="MobileOptimized" content="320">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

		<?php // icons & favicons (for more: http://www.jonathantneal.com/blog/understand-the-favicon/) ?>
		<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/apple-icon-touch.png">
		<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/favicon.png?v=2">
		<!--[if IE]>
			<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico">
		<![endif]-->
		<?php // or, set /favicon.ico for IE10 win ?>
		<meta name="msapplication-TileColor" content="#f01d4f">
		<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/win8-tile-icon.png">

		<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">

		<?php // wordpress head functions ?>
		<?php wp_head(); ?>
		<?php // end of wordpress head ?>

		<?php // drop Google Analytics Here ?>
		<?php // end analytics ?>



	</head>

	<body <?php body_class(); ?>>

    <?php
    	if (is_home()) {
    		# code...

    ?>
	<header class="header">

      <nav id="nav1" class="navbar navbar-inverse navbar-invisible navbar-static-top mcaweb-navbar-absolute">


            <div class="container">
            	<div class="row">
            		<div class="col-xs-12">
            			<!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
			            <div class="navbar-header">
			              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"  data-target="#navbar-1">
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			                <span class="icon-bar"></span>
			              </button>

			              <a class="navbar-brand" href="<?php bloginfo( 'url' ) ?>/" title="<?php bloginfo( 'name' ) ?>" rel="homepage"><?php //bloginfo('name'); ?><img style="width:118px" src="<?php echo get_template_directory_uri(); ?>/library/images/mca-logo.png"></a>

			            </div>

						<div class="searchform pull-right clear">
			            	<form action="<?php echo bloginfo('url')?>" method="get" class="form-inline">
							    <fieldset>
							    <div class="input-group">
							      <input name="s" id="search" placeholder="Search" value="<?php echo isset($_GET['s'])? $_GET['s'] : '' ?>" class="form-control" type="text">
							      <span class="input-group-btn">
							        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
							      </span>
							    </div>
							    </fieldset>
							</form>

			            </div>
			            <div class="navbar-collapse collapse navbar-responsive-collapse" id="navbar-1">
			              <?php bones_main_nav(); ?>

			            </div>

            		</div>
            	</div>
            </div>



      </nav>

	</header> <?php // end header ?>

	<div class="loading" style="position: relative; width: 100%; overflow: hidden;">
	    <div style="position: relative; left: 50%; width: 5000px; text-align: center; margin-left: -2500px;">
	        <!-- use 'margin: 0 auto;' to auto center element in parent container -->
	        <div id="slider1_container" style="position: relative; top: 0px; left: 0px; margin: 0 auto; width: 1024px; height: 768px; max-height: 100vh;">


			    <!-- Slides Container -->
			    <div u="loading" style="position: absolute; top: 0px; left: 0px;">
		            <div style="filter: alpha(opacity=90); opacity:0.9; position: absolute; display: block;
		                background-color: #000; top: 0px; left: 0px;width: 100%;height:100%;">
		            </div>
		            <div style="position: absolute; display: block; background: url(http://www.epd.gov.hk/epd/sites/all/modules/kanhan_AQHIbox/img/loading.gif) no-repeat center center;
		                top: 0px; left: 0px;width: 100%;height:100%;">
		            </div>
		        </div>
			    <div u="slides" style="cursor: move; position: absolute; overflow: hidden; left: 0px; top: 0px; width: 1024px; height: 768px;">
			        <div>
			        	<img u="image" src="<?php echo get_template_directory_uri(); ?>/library/slides/Picture1.jpg" />
			        	<!--<div u="caption" class="MCAPoemCaption" t="1" style="">
					   <p>Perhaps, I am around you...</p>
					    </div>-->
			        </div>
			        <div>
			        	<img u="image" src="<?php echo get_template_directory_uri(); ?>/library/slides/Picture2.jpg" />
			        	<!--<div u="caption" class="MCAPoemCaption" t="1" style="">
					        <!--<p>Living on streets surviving on waste...</p>
					    </div>-->
			        </div>
			        <div>
			        	<img u="image" src="<?php echo get_template_directory_uri(); ?>/library/slides/Picture3.jpg" />
			        	<!--<div u="caption" class="MCAPoemCaption" t="1" style="">
					       <p>Perhaps abandoned with unknown identity lying in a </p><p>hospital bed or by mercy of passers by...</p>
					    </div>-->
			        </div>
			        <div>
			        	<img u="image" src="<?php echo get_template_directory_uri(); ?>/library/slides/Picture4.jpg" />
			        	<!--<div u="caption" class="MCAPoemCaption" t="1" style="">
					       <p>Perhaps in dingy room working for hours and</p>
                            <p>no way to escape...</p>
					    </div>-->
			        </div>
			        <div>
			        	<img u="image" src="<?php echo get_template_directory_uri(); ?>/library/slides/Picture5.jpg" />
			        	<!--<div u="caption" class="MCAPoemCaption" t="1" style="">
					        <p>Perhaps preparing for yet another night</p>
                            <p>of tortures and abuse...</p>
					    </div>-->
			        </div>
			        <div>
			        	<img u="image" src="<?php echo get_template_directory_uri(); ?>/library/slides/Picture6.jpg" />
			        	<!--<div u="caption" class="MCAPoemCaption" t="1" style="">
					        <p>Perhaps I am around you...</p>
					    </div>-->
			        </div>
			        <div>
			        	<img u="image" src="<?php echo get_template_directory_uri(); ?>/library/slides/Picture7.jpg" />
<!--			        	<div u="caption" class="MCAPoemCaption" t="1" style="">-->
<!--					        <p>A faceless, nameless living creature...</p>-->
<!--					    </div>-->
			        </div>
			        <div>
			        	<img u="image" src="<?php echo get_template_directory_uri(); ?>/library/slides/Picture8.jpg" />
			        	<!--<div u="caption" class="MCAPoemCaption" t="1" style="">
					        <p>I remain unknown unless lucky enough</p>
                            <p>to be noted somewhere..</p>
					    </div>-->
			        </div>
			        <div>
			        	<img u="image" src="<?php echo get_template_directory_uri(); ?>/library/slides/Picture9.jpg" />
			        	<!--<div u="caption" class="MCAPoemCaption" t="1" style="">
					        <p>May be thousands of miles away...</p>
					    </div>-->
			        </div>
			        <div>
			        	<img u="image" src="<?php echo get_template_directory_uri(); ?>/library/slides/Picture10.jpg" />
			        	<!--<div u="caption" class="MCAPoemCaption" t="1" style="">
					        <p>A missing person...</p>
					    </div>-->
			        </div>

			    </div>
			</div>


	    </div>
	</div>
            <div id="demo" style="position: absolute;top: 126px">

                <section id="examples">

                    <!-- content -->
                    <div id="content-1" class="content MCAPoemCaption">

                        <p>&nbsp;</p>
                        <p>Perhaps, I am around you...</p>

                        <p>Living on streets surviving on waste...</p>

                        <p>Perhaps abandoned with unknown identity lying in a <br>
                            hospital bed or by mercy of passers by...</p>

                        <p>Perhaps in dingy room working for hours and<br>no way to escape...</p>

                        <p>Perhaps preparing for yet another night<br>of tortures and abuse...</p>

                        <p>Perhaps I am around you...</p>

                        <p>A faceless, nameless living creature...</p>

                        <p>I remain unknown unless lucky enough<br>to be noted somewhere..</p>


                        <p>May be thousands of miles away...</p>

                        <p>A missing person...</p>
                        <p>&nbsp;</p>
                    </div>
            </div>
            <!--<div class="shades">

                <div class="blurrer">

                </div>
                <div class="highlighter">

                </div>
                <div class="blurrer two">

                </div>
            </div>
-->
            <div>


                </section>

               <!-- <section id="info">
                    <p><a href="#" class="auto-scrolling-toggle">Toggle auto-scrolling</a></p>
                </section>-->
            </div>
	    <?php
	}
    ?>

<header class="header">
	<?php
		$navbar_classes;
		if (is_home()) {
			$navbar_classes = 'navbar-inverse navbar-invisible navbar-absolute-top navbar-overthetop';
		}
		elseif (is_single() && !is_home() && !is_archive()) {
			$navbar_classes = 'navbar-inverse navbar-invisible navbar-nothome navbar-fixed-top';
		}
		else $navbar_classes = 'navbar-brand-color navbar-nothome navbar-fixed-top';
	?>
	<nav class="navbar <?php echo $navbar_classes?>" id="other-menu">
          <div class="container">
            <div class="col-xs-12">
            	<!-- .navbar-toggle is used as the toggle for collapsed navbar content -->
	            <div class="navbar-header">
	              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-2">
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	                <span class="icon-bar"></span>
	              </button>

	              <!--<a class="navbar-brand<?php /*echo is_home() ? ' hidden': '' */?>" href="<?php /*bloginfo( 'url' ) */?>/" title="<?php /*bloginfo( 'name' ) */?>" rel="homepage"><?php /*//bloginfo('name'); */?><img src="<?php /*echo get_template_directory_uri(); */?>/library/images/mca-logo-inv.png"></a>-->
	              <a class="navbar-brand<?php echo is_home() ? ' hidden': '' ?>" href="<?php bloginfo( 'url' ) ?>/" title="<?php bloginfo( 'name' ) ?>" rel="homepage"><?php //bloginfo('name'); ?><img src="<?php echo get_template_directory_uri(); ?>/library/images/logo_mca.png"></a>

	            </div>
                <a class="login" href="#">Login</a>
				<div class="searchform pull-right clear">
	            	<form action="<?php echo bloginfo('url')?>" method="get" class="form-inline">
					    <fieldset>
					    <div class="input-group">
					      <input name="s" id="search" placeholder="Search" value="<?php echo isset($_GET['s'])? $_GET['s'] : '' ?>" class="form-control" type="text">
					      <span class="input-group-btn">
					        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
					      </span>
					    </div>
					    </fieldset>
					</form>
	            </div>
	            <div class="navbar-collapse collapse navbar-responsive-collapse" id="navbar-2">
	              <?php bones_second_nav(); ?>

	            </div>

            </div>
          </div>

      </nav>
</header>







