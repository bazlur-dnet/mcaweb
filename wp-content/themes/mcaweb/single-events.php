<?php get_header(); ?>

<!--     <div class="container">

			<div id="content" class="clearfix row">

				<div id="main" class="col-md-8 clearfix" role="main"> -->

<?php // get_template_part( 'breadcrumb' ); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <section class="page-banner event-detail">
        <div class="container-fluid ">
            <div class="row">

                <?php
                $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); ?>
                <section
                    <?php if (has_post_thumbnail()) { ?>
                        class="feat-head feat-image-head" style="background: linear-gradient( rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5) ), url('<?php echo $image[0]; ?>'); background-size:  50% 50%"
                    <?php
                    } else {
                        ?>
                        class="feat-head feat-no-image-head"
                    <?php
                    }
                    ?>>
                    <header class="article-header">
                        <div class="titlewrap clearfix">
                            <div class="titlewrap clearfix">
                                <h1 class="single-title entry-title"><a href="<?php the_permalink() ?>"
                                                                        rel="bookmark"
                                                                        title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
                                </h1>

                            </div>


                            <div class="postinfoshare">
                                <p class="byline vcard">


                                    <time class="updated"
                                          datetime="<?php get_the_time('Y-m-j') ?>"><?php the_time(get_option('date_format')) ?></time>
                                    <?php
                                    // var_dump(get_the_time(get_option('date_format')));
                                    ?>

                                </p>
                                <ul class="mca_shareit-bar">
                                    <li class="mca_shareit-twitter">
                                        <a target="_blank"
                                           href="http://twitter.com/home?status=ShareIt+is+a+WordPress+plugin+to+create+and+share+content+snippets%C2%A0through+social+networks%3A+Twitter%2C+Facebook%2C+Pinterest+and+Google%2B http://demo.queryloop.com/shareit/"
                                           target="_blank" title="Share on Twitter">
                                            <i class="fa fa-lg fa-twitter"></i>
                                        </a>
                                    </li>
                                    <?php $fbsharelink = "https://www.facebook.com/dialog/share?app_id=365858186958004&display=popup&href=" . urldecode(get_the_permalink()) . "&redirect_uri=" . urldecode(get_the_permalink());
                                    ?>
                                    <li class="mca_shareit-facebook"
                                        data-link="http://demo.queryloop.com/shareit/" data-name="About"
                                        data-description="ShareIt is a WordPress plugin to create and share content snippets&nbsp;through social networks: Twitter, Facebook, Pinterest and Google+">
                                        <a target="_blank" href="<?php echo $fbsharelink ?>"
                                           title="Share on Facebook">
                                            <i class="fa fa-lg fa-facebook"></i>
                                        </a>
                                    </li>
                                    <li class="mca_shareit-google">
                                        <a target="_blank"
                                           href="https://plus.google.com/share?url=http://demo.queryloop.com/shareit/"
                                           title="Share on Google+">
                                            <i class="fa fa-lg fa-google-plus"></i>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                    </header> <?php // end article header ?>
                </section>
                <?php //if ( has_post_thumbnail() ) the_post_thumbnail('full', array('class' => 'img-responsive center-block img-wide')); ?>

            </div>
        </div>
    </section>
    <div class="container event-detail">
        <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope
                 itemtype="http://schema.org/BlogPosting">


            <?php /* global $brew_options; ?>
							<?php if( $brew_options['featured'] == '2' || ( $brew_options['featured'] == '4' && is_single() ) || ( $brew_options['featured'] == '3' && is_home() ) ) { ?>
								<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-featured' ); ?>
								<?php if ( $image[1] < '750' && has_post_thumbnail() ) { ?>
									<section class="featured-content featured-img featured-img-bg" style="background: url('<?php echo $image[0]; ?>')">
								<?php } // end if
								else { ?>
									<section class="featured-content featured-img">
										<?php if ( has_post_thumbnail() ) { ?>
		                                    <a class="featured-img" href="<?php the_permalink(); ?>">
		                                    	<?php the_post_thumbnail( 'post-featured' ); ?>
		                                    </a>
			                            <?php } // end if
										else { ?>
			                            	<hr>
			                            <?php } //end else?>
				                <?php } // end else ?>
							<?php } // end if
							else { ?>
								<section class="featured-content featured-img">
							<?php } // end else ?>

							</section> */
            ?>

            <section class="entry-content single-content clearfix" itemprop="articleBody">




                <?php the_content(); ?>
                <?php wp_link_pages(
                    array(

                        'before' => '<div class="page-link"><span>' . __('Pages:', 'brew') . '</span>',
                        'after' => '</div>'
                    )
                ); ?>
            </section> <?php // end article section ?>

            <!--<footer class="article-footer single-footer clearfix">
								<span class="tags pull-left"><?php /*printf( '<span class="">' . __( 'in %1$s&nbsp;&nbsp;', 'bonestheme' ) . '</span>', get_the_category_list(', ') ); */ ?> <?php /*the_tags( '<span class="tags-title">' . __( '<i class="fa fa-tags"></i>', 'bonestheme' ) . '</span> ', ', ', '' ); */ ?></span>
              					<span class="commentnum pull-right"><a href="<?php /*comments_link(); */ ?>"><?php /*comments_number( '<i class="fa fa-comment"></i> 0', '<i class="fa fa-comment"></i> 1', '<i class="fa fa-comment"></i> %' ); */ ?></a></span>
            				</footer> --><?php /*// end article footer */ ?>


        </article> <?php // end article ?>
    </div>
    <div class="container event-detail">
        <?php comments_template(); ?>
        <div class="row">
            <div class="col-xs-12">
                <?php get_template_part('author-info'); ?>

                <?php if (is_single()) { ?>
                    <div id="single-post-nav">
                        <ul class="pager">

                            <?php $trunc_limit = 35; ?>

                            <?php if ('' != get_previous_post()) { ?>
                                <li class="previous">
                                    <?php previous_post_link('<span class="previous-page">%link</span>', __('<i class="fa fa-angle-left"></i>', 'bones') . '&nbsp;' . brew_truncate_text(get_previous_post(true)->post_title, $trunc_limit), true); ?>
                                </li>
                            <?php } // end if ?>

                            <?php if ('' != get_next_post()) { ?>
                                <li class="next">
                                    <?php next_post_link('<span class="no-previous-page-link next-page">%link</span>', '&nbsp;' . brew_truncate_text(get_next_post(true)->post_title, $trunc_limit) . '&nbsp;' . __('<i class="fa fa-angle-right"></i>', 'bones'), true) ?>
                                </li>
                            <?php } // end if ?>

                        </ul>
                    </div><!-- /#single-post-nav -->
                <?php } ?>


            </div>
        </div>
    </div>

<?php endwhile; ?>

<?php else : ?>

    <article id="post-not-found" class="hentry clearfix">
        <header class="article-header">
            <h1><?php _e('Oops, Post Not Found!', 'bonestheme'); ?></h1>
        </header>
        <section class="entry-content">
            <p><?php _e('Uh Oh. Something is missing. Try double checking things.', 'bonestheme'); ?></p>
        </section>
        <footer class="article-footer">
            <p><?php _e('This is the error message in the single.php template.', 'bonestheme'); ?></p>
        </footer>
    </article>

<?php endif; ?>

<!-- </div> --> <?php // end #main ?>

<?php // get_sidebar(); ?>

<!-- </div> --> <?php // end #content ?>

<!-- </div> --> <?php // end ./container ?>

<?php get_footer(); ?>
