<?php
/*
Template Name: Page - News and Events
*/
?>

<?php get_header(); ?>
<section id="newsandeventslist">
<div class="container hidden-xs">
    <div class="row clear">
        <div class="col-sm-offset-1 col-sm-10 col-md-offset-2 col-md-8">
            <div class="recent_posts_wrapper clear">
                <a id="list_toggle" href="#"><i class="fa fa-angle-double-up"></i></a>

                <div class="recent_posts_wrapper_inner">

                    <div class="post_list_wrapper">
                        <h3>Featured News</h3>

                        <div class="post_list_with_scroll newsblock">
                            <?php $args = array(
                                'posts_per_page' => 8,
                                'offset' => 0,
                                'category_name' => 'news',
                                'post_type' => 'post',
                                'meta_key' => '_post_featured',
                                'meta_value' => 'on'
                            );
                            $posts_array = get_posts($args); ?>
                            <?php

                            foreach ($posts_array as $post) : setup_postdata($post);
                                $thumbimg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
                                ?>
                                <a class="post_link clear" href="<?php the_permalink() ?>">
                                    <div class="imgblock"> <?php
                                        if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it.
                                            echo '<img src="' . $thumbimg[0] . '">';
                                        }
                                        ?>
                                    </div>
                                    <div class="txtblock">
                                        <p class="post_title">
                                            <?php
                                            echo wp_trim_words(get_the_title(), 10, '...');
                                            ?>
                                        </p>

                                        <p class="post_info"><?php echo get_the_date() ?></p>
                                    </div>
                                </a>

                            <?php endforeach;
                            wp_reset_postdata();

                            ?>

                        </div>
                    </div>



                    <div class="post_list_wrapper">
                        <h3>Featured Events</h3>

                        <div class="post_list_with_scroll eventsblock">
                            <?php $args = array(
                                'posts_per_page' => 8,
                                'offset' => 0,
                                'category_name' => 'events',
                                'post_type' => 'post',
                                'meta_key' => '_post_featured',
                                'meta_value' => 'on'
                            );
                            $posts_array = get_posts($args); ?>
                            <?php

                            foreach ($posts_array as $post) : setup_postdata($post);
                                $thumbimg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
                                ?>
                                <a class="post_link clear" href="<?php the_permalink() ?>">
                                    <div class="imgblock"> <?php
                                        if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it.
                                            echo '<img src="' . $thumbimg[0] . '">';
                                        }
                                        ?>
                                    </div>
                                    <div class="txtblock">
                                        <p class="post_title"><?php echo get_the_date() ?></p>
                                        <p class="post_info"><?php
                                            echo wp_trim_words(get_the_title(), 10, '...');
                                            ?></p>
                                    </div>
                                </a>

                            <?php endforeach;
                            wp_reset_postdata();

                            ?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row clear">
        <div class="col-xs-12">
            <div class="button_group_align" id="button_group_list_newsevent_filter">
                <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-primary btn-news">
                        <input type="checkbox" name="postthumboptions" id="optionnews" value="news"><i
                            class="fa fa-square-o"></i> News
                    </label>
                    <label class="btn btn-warning btn-events">
                        <input type="checkbox" name="postthumboptions" id="optionevents" value="events"><i
                            class="fa fa-square-o"></i> Events
                    </label>

                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-offset-1 col-xs-10 col-md-offset-2 col-md-8">
            <div class="row">
                <?php

                if (get_query_var('paged')) {
                    $paged = get_query_var('paged');
                } else if (get_query_var('page')) {
                    $paged = get_query_var('page');
                } else {
                    $paged = 1;
                }
                // $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
                $args = array(
                    'posts_per_page' => 6,
                    'category_name' => 'events,news',
                    'post_type' => 'post',
                    'paged' => $paged
                );
                $the_query = new WP_Query($args);
                if ($the_query->have_posts()) {

                    while ($the_query->have_posts()) {
                        $the_query->the_post();
                        $thumbimg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
                        ?>
                        <div class="col-sm-6">
                            <div class="newseventblock <?php $category = get_the_category();
                            echo $category[0]->slug;?>">
                                <a href="<?php the_permalink() ?>"><div class="imgblock" <?php
                                if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it.
                                    echo 'style="background-image: url(' . $thumbimg[0] . ')"';
                                }
                                ?> >
                                    <?php
                                    $featured = get_post_meta(get_the_ID(), '_post_featured');
                                    if ($featured[0] == 'on') {
                                        ?>
                                        <div class="featured"><i class="fa fa-bookmark"></i></div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                    </a>
                                <div class="txtblock">
                                    <div class="post_time"><?php echo get_the_date() ?></div>
                                    <div class="h3 post_title"><a href="<?php the_permalink() ?>"><?php
                                            echo wp_trim_words(get_the_title(), 10, '...');
                                            ?></a></div>
                                    <!-- <div class="post_share"><a href="#"><i class="fa fa-spin fa-spinner"></i> Share</a></div> -->
                                    <div class="post_share">
                                        <ul class="mca_share">
                                            <li class="mca_shareit-twitter">
                                                <a target="_blank"
                                                   href="http://twitter.com/home?status=ShareIt+is+a+WordPress+plugin+to+create+and+share+content+snippets%C2%A0through+social+networks%3A+Twitter%2C+Facebook%2C+Pinterest+and+Google%2B http://demo.queryloop.com/shareit/"
                                                   target="_blank" title="Share on Twitter">
                                                    <i class="fa fa-twitter"></i> Twitter
                                                </a>
                                            </li>
                                            <?php $fbsharelink = "https://www.facebook.com/dialog/share?app_id=365858186958004&display=popup&href=" . urldecode(get_the_permalink()) . "&redirect_uri=" . urldecode(get_the_permalink());
                                            ?>
                                            <li class="mca_shareit-facebook"
                                                data-link="http://demo.queryloop.com/shareit/" data-name="About"
                                                data-description="ShareIt is a WordPress plugin to create and share content snippets&nbsp;through social networks: Twitter, Facebook, Pinterest and Google+">
                                                <a target="_blank" href="<?php echo $fbsharelink ?>"
                                                   title="Share on Facebook">
                                                    <i class="fa fa-facebook"></i> Facebook
                                                </a>
                                            </li>
                                            <li class="mca_shareit-google">
                                                <a target="_blank"
                                                   href="https://plus.google.com/share?url=http://demo.queryloop.com/shareit/"
                                                   title="Share on Google+">
                                                    <i class="fa fa-google-plus"></i> Google+
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="newsevent_overlay hidden"></div>
                                </div>
                            </div>
                        </div>
                    <?php
                    }
                    ?>


                <?php

                } else {
                    // no posts found
                }
                // wpbeginner_numeric_posts_nav();
                /* Restore original Post Data */
                wp_reset_postdata();


                ?>

            </div>
            <?php
            // var_dump($the_query->max_num_pages);
            if (function_exists(bones_page_navi)) {
                bones_page_navi($the_query->max_num_pages, "", $paged);
            }
            ?>
        </div>
    </div>
</div>
<br>
</section>



<?php get_footer(); ?>
