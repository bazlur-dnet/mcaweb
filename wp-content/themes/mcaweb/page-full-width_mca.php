<?php
/*
Template Name: Page - Full Width Bootstrap MCA
*/
?>

<?php get_header(); ?>


            <!-- UNCOMMENT FOR BREADCRUMBS

            <div class="container">

        <div class="clearfix row">

          <div class="col-md-12 clearfix">
            <?php if ( function_exists('custom_breadcrumb') ) { custom_breadcrumb(); } ?>
          </div>
        </div>
      </div>  -->
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <section class="page-title">
          <div class="container">
            <div class="row">
              <div class="col-xs-12">
                <header class="page-head article-header">

                  <h1 class="page-title entry-title" itemprop="headline"><?php the_title(); ?></h1>

                </header> <!-- end article header -->
              </div>

            </div>
          </div>

      </section>
      <section class="page-banner">
        <div class="container-fluid">
              <div class="row">
                  <?php if ( has_post_thumbnail() ) the_post_thumbnail('full', array('class' => 'img-responsive center-block img-wide')); ?>

              </div>
            </div>
      </section>
      <section class="page-content">
        <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">



              <section class="clearfix" itemprop="articleBody">
                <?php the_content(); ?>


              </section> <!-- end article section -->

              <footer>

                <?php the_tags('<p class="tags"><span class="tags-title">' . __("Tags","bonestheme") . ':</span> ', ', ', '</p>'); ?>

              </footer> <!-- end article footer -->

            </article> <!-- end article -->
      </section>


      <section class="comment-section">
        <div class="container">
          <div class="row">
            <div class="col-xs-12">
              <?php comments_template('',true); ?>
            </div>
          </div>
        </div>

      </section>

            <?php endwhile; ?>

            <?php else : ?>

            <article id="post-not-found">
                <header>
                  <h1><?php _e("Not Found", "bonestheme"); ?></h1>
                </header>
                <section class="post_content">
                  <p><?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?></p>
                </section>
                <footer>
                </footer>
            </article>

            <?php endif; ?>


<?php get_footer(); ?>
