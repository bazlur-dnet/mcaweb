<footer id="footer" class="clearfix">


    <div id="sub-floor">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 wow fadeInLeft" data-wow-delay="0.3s">
                    <div class="row">
                    <div class="col-md-12 col-lg-12 copyright">
                        <ul><li>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></li>

                            <li>
                                <a href="index.php?page_id=517">Disclaimer</a>
                            </li>
                            <li>
                                <a href="index.php?page_id=519">FAQs</a>
                            </li>
                        </ul>
                    </div>
                        </div>
                </div>
                <div class="col-md-6 footer-logos wow fadeIn" data-wow-delay="0.6s">
                  <span>
                    <a href="#"><img
                            src="<?php echo get_template_directory_uri(); ?>/library/images/logos/postcode_foot_w.png"></a>
                  </span>

                  <span>
                    <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/library/images/logos/plan_foot_w.png"></a>
                  </span>
                  <span>
                    <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/library/images/logos/dnet_foot_w.png"></a>
                  </span>
                    <span>
                    <a href="#"><img
                            src="<?php echo get_template_directory_uri(); ?>/library/images/logos/saievac_foot_w.png"></a>
                  </span>
                </div>
                <div class="col-md-1 col-lg-1 col-xs-3 attribution wow fadeInRight" data-wow-delay="0.9s">
                    <a id="scrollToTop" href="#"><i class="fa fa-angle-double-up"></i> Top</a>
                </div>
            </div>
            <!-- end .row -->
        </div>
    </div>

</footer> <!-- end footer -->
<?php echo get_permalink(); ?>
<!-- all js scripts are loaded in library/bones.php -->
<?php wp_footer(); ?>
<!-- Hello? Doctor? Name? Continue? Yesterday? Tomorrow?  -->

</body>

</html> <!-- end page. what a ride! -->
