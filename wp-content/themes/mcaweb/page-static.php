<?php
/*
Template Name: Page - Static Content
*/
?>

<?php get_header(); ?>
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>


            <article id="post-<?php the_ID(); ?>" <?php post_class('clearfix'); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
                <?php
                $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );

                ?>
                <div class="container"><h1 class="page-title entry-title" itemprop="headline"><?php the_title(); ?></h1></div>
                <?php
                if($feat_image) {
                ?>
                <div class="page-banner">
                    <img src="<?php echo $feat_image;?> "/>
                </div>
                <?php
                }
                ?>
                <div class="container">

                    <div id="content" class="clearfix row">

                        <div id="main" class="col-md-12 clearfix" role="main">
              <header class="page-head article-header">



              </header> <!-- end article header -->

              <section class="page-content entry-content clearfix" itemprop="articleBody">
                <?php the_content(); ?>

              </section> <!-- end article section -->

              <footer>

                <?php the_tags('<p class="tags"><span class="tags-title">' . __("Tags","bonestheme") . ':</span> ', ', ', '</p>'); ?>

              </footer> <!-- end article footer -->

            </article> <!-- end article -->

        </div> <!-- end #content -->

    </div> <!-- end .container -->
            <?php endwhile; ?>

            <?php else : ?>

            <article id="post-not-found">
                <header>
                  <h1><?php _e("Not Found", "bonestheme"); ?></h1>
                </header>
                <section class="post_content">
                  <p><?php _e("Sorry, but the requested resource was not found on this site.", "bonestheme"); ?></p>
                </section>
                <footer>
                </footer>
            </article>

            <?php endif; ?>

          </div> <!-- end #main -->




<?php get_footer(); ?>
