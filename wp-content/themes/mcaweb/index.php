<?php get_header(); ?>

<?php $mcaweb_options = get_option('mcaweb_options');
// var_dump($mcaweb_options);
?>
<section id="mcaataglance">
    <div class="container">
        <div class="row">
                <!--<div class="col-xs-12">
                    <a href="https://www.youtube.com/watch?v=syTUY5V-AdU" id="bilkisvideoholder"></a>
                </div>
                <div class="col-xs-12">
                    <h2 class="wow fadeIn">MCA at a Glance</h2>

                    <p class="wow fadeInUp">Missing Child Alert (MCA) aims to respond to the grave issue of Child
                        Trafficking and the 'Missing Children' in South Asia, making use of Technology to design
                        interventions that encompass the entire Scope of Trafficking. The four (04) Strategy Areas are as
                        follows:-</p>
                </div>-->

            <div class="col-xs-12">

            <div class="html-code">
               <!-- <a class="popup-modal" id="bilkisvideoholder" href="#bilqis-video-modal">Open modal</a>

                <div id="bilqis-video-modal" class="white-popup-block mfp-hide">
                    <video style="width: 100%; max-width: 830px" height="auto" class="bilqis-story" src="<?php /*echo get_template_directory_uri(); */?>/library/videos/bilqis.mp4"></video>
                    <a  class="popup-modal-dismiss mfp-close" href="#"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                </div>-->
                <a href="#bilkis_video" id="bilkisvideoholder" class="open-popup-link"></a>
                <div id="bilkis_video" class="white-popup mfp-hide">
                    <video style="width: 100%; max-width: 830px" controls height="auto" id="bilqis-story" src="<?php echo get_template_directory_uri(); ?>/library/videos/bilqis.mp4"></video>
                </div>

            </div>
        </div>
        <div class="col-xs-12">
            <h2 class="wow fadeIn">MCA at a Glance</h2>

            <p class="wow fadeInUp">Missing Child Alert (MCA) aims to respond to the grave issue of Child
                Trafficking and the 'Missing Children' in South Asia, making use of Technology to design
                interventions that encompass the entire Scope of Trafficking. The four (04) Strategy Areas are as
                follows:-</p>
        </div>

        </div>
    </div>
</section

<section id="strategyarea">
    <!-- <ol class="starea-thumb-list">
					<li data-target="#carousel-strategy-area" data-slide-to="0" class="active"><img src="<?php echo get_template_directory_uri(); ?>/library/images/1.png"></li>
					<li data-target="#carousel-strategy-area" data-slide-to="1"><img src="<?php echo get_template_directory_uri(); ?>/library/images/2.png"></li>
					<li data-target="#carousel-strategy-area" data-slide-to="2"><img src="<?php echo get_template_directory_uri(); ?>/library/images/3.png"></li>
					<li data-target="#carousel-strategy-area" data-slide-to="3"><img src="<?php echo get_template_directory_uri(); ?>/library/images/4.png"></li>
				</ol> -->
    <!-- <div class="container"> -->
    <div class="container-fluid">
        <div class="row">
            <div class="col-cxs-12">
                <div id="carousel-strategy-area" class="carousel slide" data-ride="carousel">

                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li id="strategy-indicator1" data-target="#carousel-strategy-area" data-slide-to="0"
                            class="active"
                            style="background:url(<?php echo get_template_directory_uri(); ?>/library/images/1.png) ;"></li>
                        <li id="strategy-indicator2" data-target="#carousel-strategy-area" data-slide-to="1"
                            style="background:url(<?php echo get_template_directory_uri(); ?>/library/images/2.png) ;"></li>
                        <li id="strategy-indicator3" data-target="#carousel-strategy-area" data-slide-to="2"
                            style="background:url(<?php echo get_template_directory_uri(); ?>/library/images/3.png) ;"></li>
                        <li id="strategy-indicator4" data-target="#carousel-strategy-area" data-slide-to="3"
                            style="background:url(<?php echo get_template_directory_uri(); ?>/library/images/4.png) ;"></li>
                    </ol>


                    <?php
                    $args = array('posts_per_page' => 5, 'offset' => 0, 'category' => 18, 'orderby' => 'post_modified', 'order' => 'ASC');
                    $strategies = get_posts($args);
                    ?>




                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">

                        <?php
                        $count = 1;
                        foreach ($strategies as $post) : setup_postdata($post); ?>
                            <div class="item <?php echo $count == 1 ? 'active' : '' ?>">
                                <!--<img src="<?php /*echo get_template_directory_uri(); */ ?>/library/images/strategyarea/1.png"
                                     alt="...">-->
                                <?php the_post_thumbnail($size, $attr); ?>
                                <div class="info">
                                    <h3><?php echo get_the_title(); ?> </h3>

                                    <p>
                                        <?php the_excerpt(); ?> </p>
                                    <a href="index.php?page_id=98<?php echo '#strategy'.$count; ?>">Learn More &#187; </a>
                                </div>
                            </div>
                            <?php $count++; ?>
                        <?php endforeach;
                        wp_reset_postdata();
                        ?>
                        <div class="strategies-closer"><span aria-hidden="true"
                                                             class="glyphicon glyphicon-remove"></span></div>
                        <a data-slide="prev" role="button" href="#carousel-strategy-area" class="left carousel-control">
                            <span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a data-slide="next" role="button" href="#carousel-strategy-area"
                           class="right carousel-control">
                            <span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>

                    </div>

                    <!-- Controls -->
                    <!-- 				<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left"></span>
                                    </a>
                                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right"></span>
                                    </a> -->


                </div>

            </div>
        </div>
    </div>


    <!-- </div>//container  -->


</section>
<section id="patron">
    <div class="container">
        <div class="row">
            <div class="col-xs-4 col-sm-3">
                <p class="patron-logo">
                    <a href="index.php?page_id=108"><img
                            src="<?php echo get_template_directory_uri(); ?>/library/images/logos/postcode.png"
                            alt="..."></a>
                </p>
            </div>
            <div class="col-xs-4 col-sm-2">
                <p class="patron-logo">
                    <a href="index.php?page_id=108"><img
                            src="<?php echo get_template_directory_uri(); ?>/library/images/logos/plan.png"
                            alt="..."></a>
                </p>
            </div>
            <div class="col-xs-offset-2 col-sm-offset-0 col-xs-4 col-sm-3">
                <p class="patron-logo">
                    <a href="index.php?page_id=108"><img
                            src="<?php echo get_template_directory_uri(); ?>/library/images/logos/dnet.png"
                            alt="..."></a>
                </p>
            </div>
            <div class="col-xs-8 col-sm-4">
                <p class="patron-logo">
                    <a href="index.php?page_id=108"><img
                            src="<?php echo get_template_directory_uri(); ?>/library/images/logos/saievac.png"
                            alt="..."></a>
                </p>
            </div>
        </div>
    </div>
</section>


<section id="newsevents">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="button_group_align wow fadeInDown" id="button_group_home_newsevents_filter">
                    <div class="btn-group" data-toggle="buttons">
                        <label class="btn btn-primary active news">
                            <input type="radio" name="postthumboptions" id="optionnews" autocomplete="off" value="news">
                            News
                        </label>
                        <label class="btn btn-warning events">
                            <input type="radio" name="postthumboptions" id="optionevents" autocomplete="off"
                                   value="events" checked> Events
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="" id="eventsblock">
        <div class="">
            <?php $args = array(
                'posts_per_page' => 8,
                'offset' => 0,
                'category_name' => 'events',
                'post_type' => 'post',
            );
            $posts_array = get_posts($args); ?>
            <div class=""><?php //var_dump($posts_array) ?>
            </div>
            <div class="hide">
                <div id="slider2_container" class=""
                     style="position: relative; top: 0px; left: 0px; width: 1024px; height: 160px; overflow: hidden; margin:0 auto;">

                    <!-- Loading Screen -->
                    <div u="loading" style="position: absolute; top: 0px; left: 0px;">
                        <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
			                background-color: #000; top: 0px; left: 0px;width: 100%;height:100%;">
                        </div>
                        <div style="position: absolute; display: block; background: url(http://www.epd.gov.hk/epd/sites/all/modules/kanhan_AQHIbox/img/loading.gif) no-repeat center center;
			                top: 0px; left: 0px;width: 100%;height:100%;">
                        </div>
                    </div>

                    <!-- Slides Container -->
                    <div u="slides"
                         style="cursor: grab; position: absolute; left: 0px; top: 0px; width: 100%; height: 160px; overflow: hidden;">
                        <?php
                        $count = 1;
                        foreach ($posts_array as $post) : setup_postdata($post); ?>
                            <div class="item"
                                style="background:url(<?php $thumbimg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
                                echo $thumbimg[0] ?>); background-size:cover;"><?php
                                if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it.
                                    //the_post_thumbnail();
                                }
                                ?>
                                <!-- <img u="image" src="<?php $thumbimg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
                                echo $thumbimg[0] ?>" /> -->
                                <div class="newseventcaption">
                                    <label class="date"><?php echo $count++ ?></label>
                                <a u="caption" class="" t="1" style=""
                                   href="<?php the_permalink(); ?>">
                                    <?php wp_trim_words(the_title(), 4, '...'); ?>

                                </a>
                                </div>
                            </div>
                        <?php

                            endforeach;
                        ?> <?php
                        $count = 1;
                        foreach ($posts_array as $post) : setup_postdata($post); ?>
                            <div class="item"
                                style="background:url(<?php $thumbimg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
                                echo $thumbimg[0] ?>); background-size:cover;"><?php
                                if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it.
                                    //the_post_thumbnail();
                                }
                                ?>
                                <!-- <img u="image" src="<?php $thumbimg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
                                echo $thumbimg[0] ?>" /> -->

                                <div class="newseventcaption">
                                    <label class="date"><?php echo $count++ ?></label>
                                <a u="caption" class="" t="1" style=""
                                   href="<?php the_permalink(); ?>">
                                    <?php wp_trim_words(the_title(), 4, '...'); ?>

                                </a>
                                </div>
                            </div>
                        <?php endforeach;
                        wp_reset_postdata();

                        ?>

                    </div>

                    <!-- Bullet Navigator Skin Begin -->
                    <style>

                    </style>
                    <!-- bullet navigator container -->
                    <div u="navigator" class="jssorb03" style="position: absolute; bottom: 4px; right: 6px;">
                        <!-- bullet navigator item prototype -->
                        <div u="prototype"
                             style="position: absolute; width: 21px; height: 21px; text-align:center; line-height:21px; color:white; font-size:12px;">
                            <div u="numbertemplate"></div>
                        </div>
                    </div>
                    <!-- Bullet Navigator Skin End -->

                    <!-- Arrow Navigator Skin Begin -->
                    <style>

                    </style>
                    <!-- Arrow Left -->
			        <span u="arrowleft" class="jssora03l" style="width: 55px; height: 55px; top: 123px; left: 8px;">
			        </span>
                    <!-- Arrow Right -->
			        <span u="arrowright" class="jssora03r" style="width: 55px; height: 55px; top: 123px; right: 8px">
			        </span>

                </div>
            </div>
            <?php $args = array(
                'posts_per_page' => 8,
                'offset' => 0,
                'category_name' => 'news',
                'post_type' => 'post',
            );
            $posts_array = get_posts($args); ?>
            <div class="">
                <div id="slider3_container"
                     style="position: relative; top: 0px; left: 0px; width: 1024px; height: 160px; overflow: hidden; margin:0 auto;">

                    <!-- Loading Screen -->
                    <div u="loading" style="position: absolute; top: 0px; left: 0px;">
                        <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
			                background-color: #000; top: 0px; left: 0px;width: 100%;height:100%;">
                        </div>
                        <div style="position: absolute; display: block; background: url(http://www.epd.gov.hk/epd/sites/all/modules/kanhan_AQHIbox/img/loading.gif) no-repeat center center;
			                top: 0px; left: 0px;width: 100%;height:100%;">
                        </div>
                    </div>

                    <!-- Slides Container -->
                    <div u="slides"
                         style="cursor: grab; position: absolute; left: 0px; top: 0px; width: 100%; height: 160px; overflow: hidden;">
                        <?php
                        $count = 1;
                        foreach ($posts_array as $post) : setup_postdata($post); ?>
                            <div class="item"
                                style="background:url(<?php $thumbimg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
                                echo $thumbimg[0] ?>); background-size:cover;"><?php
                                if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it.
                                    //the_post_thumbnail();
                                }
                                ?>
                                <!-- <img u="image" src="<?php $thumbimg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
                                echo $thumbimg[0] ?>" /> -->
                                <div u="caption" class="newseventcaption" t="1" style="">
                                    <label class="date"><?php echo $count++ ?></label>
                                    <a href="<?php the_permalink(); ?>"><?php wp_trim_words(the_title(), 4, '...'); ?></a>
                                </div>
                            </div>
                        <?php endforeach;


                        ?><?php
                        $count = 1;
                        foreach ($posts_array as $post) : setup_postdata($post); ?>
                            <div
                                style="background:url(<?php $thumbimg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
                                echo $thumbimg[0] ?>); background-size:cover;"><?php
                                if (has_post_thumbnail()) { // check if the post has a Post Thumbnail assigned to it.
                                    //the_post_thumbnail();
                                }
                                ?>
                                <!-- <img u="image" src="<?php $thumbimg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'medium');
                                echo $thumbimg[0] ?>" /> -->
                                <div u="caption" class="newseventcaption" t="1" style="">
                                    <label class="date"><?php echo $count++ ?></label>
                                    <a href="<?php the_permalink(); ?>"><?php wp_trim_words(the_title(), 4, '...'); ?></a>
                                </div>
                            </div>
                        <?php endforeach;
                        wp_reset_postdata();

                        ?>


                    </div>

                    <!-- Bullet Navigator Skin Begin -->
                    <style>

                    </style>
                    <!-- bullet navigator container -->
                    <div u="navigator" class="jssorb03" style="position: absolute; bottom: 4px; right: 6px;">
                        <!-- bullet navigator item prototype -->
                        <div u="prototype"
                             style="position: absolute; width: 21px; height: 21px; text-align:center; line-height:21px; color:white; font-size:12px;">
                            <div u="numbertemplate"></div>
                        </div>
                    </div>
                    <!-- Bullet Navigator Skin End -->

                    <!-- Arrow Navigator Skin Begin -->
                    <style>

                    </style>
                    <!-- Arrow Left -->
			        <span u="arrowleft" class="jssora03l" style="width: 55px; height: 55px; top: 123px; left: 8px;">
			        </span>
                    <!-- Arrow Right -->
			        <span u="arrowright" class="jssora03r" style="width: 55px; height: 55px; top: 123px; right: 8px">
			        </span>

                </div>
            </div>
        </div>

    </div>

</section>


<section class="getinvolved">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12 wow fadeInUp">
                <a class="get_involved" href="?p=329">
                    <h1><i class="fa fa-chevron-circle-right"></i></h1>

                    <h3>Raise Your Voice Against Child Trafficking. Get Involved.</h3>
                </a>
            </div>
        </div>
    </div>

</section>
<section class="form">
    <img style="width:100%" src="<?php echo get_template_directory_uri(); ?>/library/images/formdivider.png">

    <div class="container">
        <!--<h3 id="come-and-join-us">Come & Join Us</h3>

    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-7 wow fadeInLeft">
            <p>If you are interested to support MCA, you may wish to contact with us.</p>
            <?php
        /*            echo do_shortcode("[contact-form]
                    <div class='row'>
                        <div class='col-xs-12 col-sm-6'>
                            <div class='form-group'>
                               [contact-field label='Full Name' placeholder='Full Name*' type='name' required='1'/]
                           </div>
                            <div class='form-group'>
                               [contact-field label='Company/University' placeholder='Company/University' type='url' /]
                           </div>
                        </div>
                        <div class='col-xs-12 col-sm-6'>
                            <div class='form-group'>
                              [contact-field label='Write to Us' placeholder='Write to Us*' type='textarea' required='1'/]
                           </div>
                       </div>
                   </div>
                   <div class='row'>
                       <div class='col-xs-12 col-sm-6'>
                            <div class='form-group'>
                              [contact-field label='Work Email' placeholder='Work Email*' type='email' required='1'/]
                           </div>
                       </div>
                   <div class='col-xs-12 col-sm-6'>
                        <div class='form-group'>
                            [/contact-form]");

                    */
        ?>
            </div>
        </div>
        </div>
    </div>-->
        <div class="col-xs-12 col-sm-4 col-lg-12 wow fadeInRight" data-wow-delay="0.8s">
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <div class="row">
                        <?php

                        ?>

                        <div class="col-lg-6 col-lg-offset-3 col-xs-6 col-xs-offset-6">
                            <div class="form-horizontal">
                                <p class="text-left">To know what’s going on, Please Subscribe</p>

                                <?php echo do_shortcode('[mc4wp_form]') ?>

                            </div>
                        </div>

                    </div>
                </div>
                <!--<div class="col-lg-4 col-xs-6 col-lg-offset-4">
                    <div class="row">
                        <div class="col-xs-12"><p style="margin-top: 5px; text-align: right">Connect</p></div>

                        <div class="col-xs-3">
                            <a href="#" class="btn btn-forminverse form-control share-buttons"><i
                                    class="fa fa-lg fa-facebook"></i></a>
                        </div>

                        <div class="col-xs-3">
                            <a href="#" class="btn btn-forminverse form-control share-buttons"><i
                                    class="fa fa-lg fa-twitter"></i></a>
                        </div>

                        <div class="col-xs-3">
                            <a href="#" class="btn btn-forminverse form-control share-buttons"><i
                                    class="fa fa-lg fa-youtube-play"></i></a>
                        </div>

                        <div class="col-xs-3">
                            <a href="#" class="btn btn-forminverse form-control share-buttons"><i
                                    class="fa fa-lg fa-google-plus"></i></a>
                        </div>

                    </div>
                </div>-->
            </div>
        </div>
    </div>
    </div>


    <!--<h3>Come & Join Us</h3>-->


    </div>
    </div>
    </div>
</section>
<section class="google-map_s">

</section>

<?php get_footer(); ?>
